import math

class Circle(object):
	def __init__(self, radius):
		self.radius = radius

	def get_Area(self):
		self.area = math.pi*self.radius**2
		return self.area